package nctuw.littledot.localtreasure;

import android.location.Location;

public interface ExpeditionListener {
	void onDestinationArrival();

	void onLocationChanged(Location location);
}
