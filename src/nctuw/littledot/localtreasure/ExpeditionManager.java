package nctuw.littledot.localtreasure;

import nctuw.littledot.util.Echo;
import nctuw.littledot.util.Leg;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler.Callback;
import android.text.format.Time;

public class ExpeditionManager implements LocationListener, SensorEventListener {
	public static final long		LOCATION_TIME_THRESH	= 5 * 60 * 1000;
	public static final float		LOCATION_ACC_THRESH		= 50F;

	private Context							mContext;
	private LocationManager			mLM;
	private SensorManager				mSM;

	private Callback						mCallback;
	private ExpeditionListener	mExpListener;

	private int									mDistance;
	private double							mBearing;
	private Location						mCurLocation;
	private Location						mDestLocation;
	private boolean							mRecalibrate					= true;
	private boolean							atDest								= false;

	private float								mAcceler[];
	private float								mMagnetic[];

	private Location						lkGPS;
	private Location						lkNetwork;
	private Location						lkPassive;

	public ExpeditionManager(Context context, int distance) {
		mContext = context;
		mDistance = distance;
	}

	public ExpeditionManager(Context context, int distance, double bearing,
			Location dest) {
		mContext = context;
		mDistance = distance;
		mBearing = bearing;
	}

	private void init() {
		// get a fresh start, only concerns are time & accuracy
		Time time = new Time();
		time.setToNow();
		long now = time.toMillis(false);
		long seconds;

		lkGPS = mLM.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		// time freshness must < 5 minutes & accuracy must < 50m
		if (lkGPS != null && now - lkGPS.getTime() < LOCATION_TIME_THRESH
				&& lkGPS.getAccuracy() < LOCATION_ACC_THRESH) {
			mCurLocation = lkGPS;
			Echo.d(mContext, "gps=\n" + lkGPS);
		}

		lkNetwork = mLM.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		if (lkNetwork != null
				&& ((mCurLocation != null
						&& lkNetwork.getTime() > mCurLocation.getTime() && lkNetwork
						.getAccuracy() < LOCATION_ACC_THRESH) || (mCurLocation == null
						&& now - lkNetwork.getTime() < LOCATION_TIME_THRESH && lkNetwork
						.getAccuracy() < LOCATION_ACC_THRESH))) {
			mCurLocation = lkNetwork;
			Echo.d(mContext, "network=\n" + lkNetwork);
		}

		lkPassive = mLM.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

		if (lkPassive != null
				&& ((mCurLocation != null
						&& lkPassive.getTime() > mCurLocation.getTime() && lkPassive
						.getAccuracy() < LOCATION_ACC_THRESH) || (mCurLocation == null
						&& now - lkPassive.getTime() < LOCATION_TIME_THRESH && lkPassive
						.getAccuracy() < LOCATION_ACC_THRESH))) {
			mCurLocation = lkPassive;
			Echo.d(mContext, "passive=\n" + lkPassive);
		}

		// randomize direction, calculate destination if current location is set
		mBearing = Math.random() * 360;
		if (mCurLocation != null) {
			mDestLocation = Geodesy.calculateVincentyDestination(mCurLocation,
					mBearing, mDistance);
		}

		// register location updates
		for (String provider : mLM.getAllProviders()) {
			Leg.d("provider=" + provider);

			mLM.requestLocationUpdates(provider, 0, mDistance * 0.05 < 10 ? 10
					: (int) (mDistance * 0.05), this);
		}

		// register sensor updates
		mSM.registerListener(this, mSM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_UI);
		mSM.registerListener(this,
				mSM.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_UI);
	}

	public void setOnDestinationArrivalListener(Callback callback) {
		mCallback = callback;
	}

	/********************************/
	/** Interface LocationListener **/
	/********************************/

	public void onLocationChanged(Location location) {
		Leg.d("location=" + location.toString());
		if (location.getAccuracy() > LOCATION_ACC_THRESH) {
			Leg.d("Discarded inaccurate location=" + location.getAccuracy());
			return;
		}

		mCurLocation = location;

		// always recalibrate on first gps lock
		if (mDestLocation == null || mRecalibrate) {
			mDestLocation = Geodesy.calculateVincentyDestination(location, mBearing,
					mDistance);
			// greenMarker.addLocation(mDestLocation, "recali-Dest",
			// mDestLocation.toString());
			// tvDestLoc.setText("===destLoc===\n" + mDestLocation.toString());

			mRecalibrate = false;

			// if (pdLock.isShowing()) {
			// pdLock.dismiss();
			// }
		}

		if (mCurLocation.distanceTo(mDestLocation) < (float) (mDistance * 0.05)) {
			atDest = true;
			// greenMarker.addLocation(mCurLocation, "CurLoc",
			// mCurLocation.toString());
		} else if (mCurLocation.getProvider().equals(LocationManager.GPS_PROVIDER)) {
			// blueMarker.addLocation(mCurLocation, "CurLoc",
			// mCurLocation.toString());
		} else {
			// pinkMarker.addLocation(mCurLocation, "CurLoc",
			// mCurLocation.toString());
		}
		// tvDistance.setText("===dist===" +
		// mCurLocation.distanceTo(mDestLocation));
	}

	public void onProviderDisabled(String provider) {

	}

	public void onProviderEnabled(String provider) {

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	/***********************************/
	/** Interface SensorEventListener **/
	/***********************************/

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	public void onSensorChanged(SensorEvent event) {
		int type = event.sensor.getType();
		if (type == Sensor.TYPE_ACCELEROMETER) {
			System.arraycopy(event.values, 0, mAcceler, 0, 3);
		} else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			System.arraycopy(event.values, 0, mMagnetic, 0, 3);
		}

		float[] r = new float[9], bearing = new float[3];
		if (SensorManager.getRotationMatrix(r, null, mAcceler, mMagnetic)) {
			SensorManager.getOrientation(r, bearing);
			// tvBearing.setText(String.format("x=%.5f y=%.5f z=%.5f", bearing[0],
			// bearing[1], bearing[2]));
		}
	}

}
