package nctuw.littledot.localtreasure;

import java.util.ArrayList;

import org.holoeverywhere.app.AlertDialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

public class MapOverlay extends ItemizedOverlay<OverlayItem> {

	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	private Context mContext;

	public MapOverlay(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
		populate();
	}

	public MapOverlay(Drawable defaultMarker, Context context) {
		super(boundCenterBottom(defaultMarker));
		mContext = context;
		populate();
	}

	public void addLocation(Location location, String title, String snippet) {
		GeoPoint point;

		if (location != null) {
			point = new GeoPoint((int) (location.getLatitude() * 1000000F),
					(int) (location.getLongitude() * 1000000F));
			OverlayItem overlayitem = new OverlayItem(point, title, snippet);
			addOverlay(overlayitem);
		}
	}

	public void clearOverlay() {
		mOverlays.clear();
		populate();
	}

	/*
	 * Class ItemizedOverlay (non-Javadoc)
	 * 
	 * @see com.google.android.maps.ItemizedOverlay#size()
	 */

	@Override
	public int size() {
		return mOverlays.size();
	}

	@Override
	protected OverlayItem createItem(int i) {
		// When the populate() method executes, it will call createItem(int) in the
		// ItemizedOverlay to retrieve each OverlayItem. You must override this
		// method to properly read from the ArrayList and return the OverlayItem
		// from the position specified by the given integer.
		return mOverlays.get(i);
	}

	public void addOverlay(OverlayItem overlay) {
		mOverlays.add(overlay);

		// Each time you add a new OverlayItem to the ArrayList, you must call
		// populate() for the ItemizedOverlay, which will read each of the
		// OverlayItem objects and prepare them to be drawn.

		populate();
	}

	@Override
	protected boolean onTap(int index) {
		OverlayItem item = mOverlays.get(index);
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet());
		dialog.show();
		return true;
	}

}
