package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.R;

import org.holoeverywhere.preference.PreferenceActivity;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;

import android.os.Bundle;
import android.view.View;

public class SettingsActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Holo_Theme);
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings_act);

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setTitle("Settings");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
			return true;

		}

		return super.onOptionsItemSelected(item);
	}

}
