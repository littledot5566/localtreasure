package nctuw.littledot.localtreasure.components;

import java.text.NumberFormat;

import nctuw.littledot.localtreasure.Const;
import nctuw.littledot.localtreasure.R;
import nctuw.littledot.localtreasure.database.DB;
import nctuw.littledot.localtreasure.database.Profile;
import nctuw.littledot.util.Leg;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ProfileActivity extends SherlockActivity implements TabListener {
	// widgets
	private TextView tvName;
	private EditText etName;
	private TextView tvExpeditionTotal;
	private TextView tvTotalDistance;
	private TextView tvTimeTotal;
	private GridView gvTreasure;

	private DB mDB;
	private Profile mProfile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.profile_activity);
		tvName = (TextView) findViewById(R.id.profile_tv_name);
		etName = (EditText) findViewById(R.id.profile_et_name);
		tvExpeditionTotal = (TextView) findViewById(R.id.profile_tv_totalExpedition);
		tvTotalDistance = (TextView) findViewById(R.id.profile_tv_totalDistance);
		tvTimeTotal = (TextView) findViewById(R.id.profile_tv_totalTime);
		gvTreasure = (GridView) findViewById(R.id.profile_gv_treasure);

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setTitle("Explorer Profile");

		Bundle bundle = getIntent().getExtras();
		mProfile = bundle.getParcelable(Const.BUNDLE_PROFILE);
		Leg.a(mProfile.toString());

		mDB = new DB(this).open(DB.MODE_WRITE);

		NumberFormat nf = NumberFormat.getInstance();

		tvName.setText(mProfile.getName());
		tvExpeditionTotal.setText("Total expeditions="
				+ mProfile.getTotalExpeditions());
		tvTotalDistance.setText("Total distance travelled="
				+ nf.format(mProfile.getTotalDistance()) + "m");

		// Support Action Bar (future work)
		// ActionBar ab = getSupportActionBar();
		// ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// ab.addTab(ab.newTab().setText("Profile").setTabListener(this));
		// ab.addTab(ab.newTab().setText("History").setTabListener(this));
	}

	@Override
	protected void onResume() {
		super.onResume();
		mDB.open(DB.MODE_WRITE);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mDB.close();
	}

	private static final int EDIT = 1;
	private static final int SAVE = 2;
	private int isEdit = 0;
	private Menu menu;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;

		menu.add(Menu.NONE, EDIT, Menu.NONE, "Edit")
				.setIcon(android.R.drawable.ic_menu_edit)
				.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add(Menu.NONE, SAVE, Menu.NONE, "Save")
				.setIcon(android.R.drawable.ic_menu_save)
				.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
				.setVisible(false);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
			return true;

		} else if (id == EDIT) {
			tvName.setVisibility(View.GONE);
			etName.setVisibility(View.VISIBLE);
			etName.setText(tvName.getText());

			menu.findItem(EDIT).setVisible(false);
			menu.findItem(SAVE).setVisible(true);

			return true;

		} else if (id == SAVE) {
			etName.setVisibility(View.GONE);
			tvName.setVisibility(View.VISIBLE);
			tvName.setText(etName.getText());

			mProfile.setName(etName.getText().toString());
			mDB.updateProfile(mProfile);

			menu.findItem(EDIT).setVisible(true);
			menu.findItem(SAVE).setVisible(false);

			return true;

		}

		return super.onOptionsItemSelected(item);
	}

	/***************************/
	/** Interface TabListener **/
	/***************************/

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

	}

}
