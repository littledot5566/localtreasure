package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.R;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.ListView;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;

public class TreasureActivity extends Activity {

	private TextView tvExcuse;
	private ListView lvTreasure;

	private TreasureActivityAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.treasure_act);
		tvExcuse = (TextView) findViewById(R.id.chest_tv_empty);
		lvTreasure = (ListView) findViewById(R.id.chest_lv_frame);

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setTitle("Treasure Chest");

		mAdapter = new TreasureActivityAdapter(this);
		mAdapter.registerDataSetObserver(new DataSetObserver() {
			@Override
			public void onChanged() {
				super.onChanged();
				checkEmpty();
			}
		});
		lvTreasure.setAdapter(mAdapter);

		checkEmpty();
	}

	private void checkEmpty() {
		if (mAdapter.getCount() == 0)
			tvExcuse.setVisibility(View.VISIBLE);
		else
			tvExcuse.setVisibility(View.GONE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}
}
