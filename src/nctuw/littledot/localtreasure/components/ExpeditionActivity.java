package nctuw.littledot.localtreasure.components;

import java.text.NumberFormat;

import nctuw.littledot.localtreasure.Const;
import nctuw.littledot.localtreasure.Geodesy;
import nctuw.littledot.localtreasure.MapOverlay;
import nctuw.littledot.localtreasure.R;
import nctuw.littledot.localtreasure.database.DB;
import nctuw.littledot.localtreasure.database.Expedition;
import nctuw.littledot.localtreasure.database.Route;
import nctuw.littledot.localtreasure.database.SP;
import nctuw.littledot.localtreasure.database.Treasure;
import nctuw.littledot.util.Leg;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockMapActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class ExpeditionActivity extends SherlockMapActivity implements
		LocationListener, SensorEventListener {
	public static final long LOCATION_TIME_THRESH = 5 * 60 * 1000;
	public static final float LOCATION_ACC_THRESH = 100.0f;
	public static final int LOCATION_LOCK_DIALOG = 1;
	public static final int EXPEDITION_COMPLETE_DIALOG = 0;
	public static final int GIVE_UP_DIALOG = 2;
	public static final int INFO_DIALOG = 3;

	public static final String BUND_EXPEDITION = "expedition";

	private LocationManager mLM;
	private SensorManager mSM;
	private DB mDB;
	private SP mSP;

	private MapView map;

	private ProgressDialog pdLock;

	// game states
	private boolean isCalibrated = false;
	private boolean isAtDest = false;

	private float mAcceler[] = new float[3];
	private float mMagnetic[] = new float[3];
	private MapOverlay destMarker; // destination
	private MapOverlay playerMarker; // player
	private MapOverlay startMarker; // start
	private MapController mController;

	private Expedition mExpedition;
	private Route mRoute;

	// private ExpeditionManager ell;

	/*
	 * Base class Activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Const.THEME);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.expedition_act);

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setTitle("Local Treasure");

		// mLI = (LayoutInflater)
		// getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mLM = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		mSM = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mDB = new DB(this).open(DB.MODE_WRITE);
		mSP = new SP(this);

		// initialize map widgets
		map = (MapView) findViewById(R.id.mapview);
		mController = map.getController();

		pdLock = new ProgressDialog(this);

		map.setBuiltInZoomControls(true);

		map.getOverlays().add(
				destMarker = new MapOverlay(getResources().getDrawable(
						R.drawable.green_map_marker_32_32), this));
		map.getOverlays().add(
				playerMarker = new MapOverlay(getResources().getDrawable(
						R.drawable.pink_map_marker_32_32), this));
		map.getOverlays().add(
				startMarker = new MapOverlay(getResources().getDrawable(
						R.drawable.blue_map_marker_32_32), this));

		// initialize Expedition
		if (savedInstanceState != null) {
			mExpedition = savedInstanceState.getParcelable(BUND_EXPEDITION);
			isCalibrated = true;
			Leg.a(String.format("bundle=%s", mExpedition.toString()));

		} else {
			// resume or start a new Expedition
			Bundle extras = getIntent().getExtras();
			String source = extras.getString(Const.BUNDLE_SOURCE);

			if (source.equals(ExpeditionConfigActivity.class.getName())) {
				// initialize new Expedition
				mExpedition = new Expedition();
				mExpedition.setPID(mSP.getActiveProfileID());
				mExpedition.setStartTS(System.currentTimeMillis());
				mExpedition.setDistance(extras.getDouble(Const.BUNDLE_DISTANCE,
						0));
				mExpedition.setDistanceLeft(mExpedition.getDistance());
				mExpedition.setMode(extras.getInt(Const.BUNDLE_MODE));

				mRoute = new Route();

			} else if (source.equals(MainActivity.class.getName())
					|| source.equals(HistoryActivity.class.getName())) {
				// load saved Expedition
				long eID = extras.getLong(Const.BUNDLE_EID);
				mExpedition = mDB.queryExpedition(eID);
				mRoute = mDB.queryRoute(eID);

				startMarker.clearOverlay();
				startMarker.addLocation(
						// mExpedition.getStartLoc(),
						mRoute.getNextWaypoint(),
						"Start",
						"Reach the destination and claim your prize.");
				destMarker.clearOverlay();
				destMarker.addLocation(
						// mExpedition.getDestLoc(),
						mRoute.getWaypoint(0),
						"Destination",
						"There seems to be something shimmering over there!");

				mController.setCenter(new GeoPoint(
						// (int) (mExpedition.getDestLoc().getLatitude() * 1E6),
						(int) (mRoute.getNextWaypoint().getLatitude() * 1E6),
						// (int) (mExpedition.getDestLoc().getLongitude() * 1E6)));
						(int) (mRoute.getNextWaypoint().getLongitude() * 1E6)));
				mController.setZoom(14);

				isCalibrated = true;

				Toast.makeText(this,
						"Please walk around so that the GPS can get a fix",
						Toast.LENGTH_LONG).show();
			}
			Leg.a(mExpedition.toString());

			if (!isCalibrated)
				showDialog(LOCATION_LOCK_DIALOG);

		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		// open database
		mDB.open(DB.MODE_WRITE);

		// register location updates
		for (String provider : mLM.getAllProviders()) {
			Leg.a("provider=" + provider);

			if (!provider.equals(LocationManager.PASSIVE_PROVIDER))
				mLM.requestLocationUpdates(provider, 3000, 0, this);
		}
		// mLM.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, this);
		// mLM.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 0,
		// this);

		// register sensor updates
		mSM.registerListener(this,
				mSM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_UI);
		mSM.registerListener(this,
				mSM.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Leg.a("[exp]:" + mExpedition);

		// mExpedition.setEndTS(System.currentTimeMillis());

		// save only after destination is confirmed
		if (mRoute.size() > 1) {
			mDB.updateExpedition(mExpedition);
			// only for incompleted expeditions
			if (!isAtDest)
				mSP.setActiveExpeditionID(mExpedition.getEID());
		}

		mDB.close();
		mLM.removeUpdates(this);
		mSM.unregisterListener(this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (isCalibrated) {
			outState.putParcelable(BUND_EXPEDITION, mExpedition);
		}

		Leg.a(String.format("expedition=%s", mExpedition.toString()));
	}

	public void onClick(View v) {
		int id = v.getId();
	}

	private static int MENU_GIVEUP = 1;
	private static int MENU_INFO = 2;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(Menu.NONE, MENU_INFO, Menu.NONE, "Info")
				.setIcon(android.R.drawable.ic_menu_info_details)
				.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add(Menu.NONE, MENU_GIVEUP, Menu.NONE, "Edit")
				.setIcon(android.R.drawable.ic_menu_myplaces)
				.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		} else if (id == MENU_GIVEUP) {
			showDialog(GIVE_UP_DIALOG);
		} else if (id == MENU_INFO) {
			showDialog(INFO_DIALOG);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {

		if (id == LOCATION_LOCK_DIALOG) {
			pdLock.setTitle("Calibrating device");
			pdLock.setMessage("Please walk around so that the GPS can get a fix");
			pdLock.setCanceledOnTouchOutside(false);
			pdLock.setCancelable(true);
			pdLock.setOnCancelListener(new OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					dialog.dismiss();
					finish();
				}
			});
			return pdLock;

		} else if (id == EXPEDITION_COMPLETE_DIALOG) {
			long seconds = (mExpedition.getEndTS() - mExpedition.getStartTS()) / 1000;

			String message;
			if (mExpedition.getPrize() != null) {
				message = String
						.format(
								"You have come a long way to complete this expedition!\n\n"
										+ "You have found some %s in your travels.\n\n"
										+ "Distance travelled: %f meters\n"
										+ "Time taken: %d seconds\n",
								mExpedition.getPrize().getName(),
								mExpedition.getTravelled(),
								seconds);
			}
			else {
				message = String
						.format(
								"You have come a long way to complete this expedition!\n\n"
										+ "Distance travelled: %f meters\n"
										+ "Distance from destination: %f meters\n"
										+ "Time taken: %d seconds\n",
								mExpedition.getTravelled(),
								mExpedition.getDistanceLeft(), seconds);
			}

			// Use the Builder class for convenient dialog construction
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setTitle("Congratulations!")
					.setMessage(message)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.dismiss();
									finish();
								}
							});

			return builder.create();

		} else if (id == GIVE_UP_DIALOG) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setTitle("It's getting pretty late...")
					.setMessage("Conclude the expedition?")
					.setNegativeButton("No", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					})
					.setPositiveButton("Yes", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							concludeExpedition();
							dialog.dismiss();
							finish();
						}
					});
			return builder.create();
		} else if (id == INFO_DIALOG) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("The journey thus far").setMessage("");
			return builder.create();
		}

		return super.onCreateDialog(id, args);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
		super.onPrepareDialog(id, dialog, args);
		Leg.a("id=" + id);
		if (id == INFO_DIALOG) {

			NumberFormat nf = NumberFormat.getInstance();

			TextView tvMsg = (TextView) dialog.findViewById(R.id.message);
			tvMsg.setText("Distance travelled: "
					+ nf.format(mExpedition.getTravelled()) + "m\n"
					+ "Remaining distance: "
					+ nf.format(mExpedition.getDistanceLeft()) + "m");
		}
	}

	/********************************/
	/** Interface LocationListener **/
	/********************************/

	public void onLocationChanged(Location location) {
		Leg.a("location=" + location.toString());

		if (isAtDest) {
			Leg.a("Ignoring repeated location update.");
			return;
		}

		if (location.getAccuracy() > LOCATION_ACC_THRESH) {
			Leg.a("Discarded inaccurate location=" + location.getAccuracy());
			return;
		}

		// mCurLocation = location;

		playerMarker.clearOverlay();
		playerMarker.addLocation(location, null, "You are here.");

		// always recalibrate on first location lock
		if (!isCalibrated) {

			double bearing = Math.random() * 360;
			// mExpedition.setStartLoc(location);
			// mExpedition.setDestLoc(Geodesy.calculateVincentyDestination(
			// location, bearing, mExpedition.getDistance()));

			// mExpedition.addWaypoint(location);
			mRoute.addWaypoint(location);

			if (mExpedition.getMode() == Const.MODE_ONEWAY) {
				mRoute.addWaypoint(Geodesy.calculateVincentyDestination(
						location, bearing, mExpedition.getDistance()));
			} else if (mExpedition.getMode() == Const.MODE_TOANDFRO) {
				mRoute.addWaypoint(Geodesy.calculateVincentyDestination(
						location, bearing, mExpedition.getDistance() / 2));
				mRoute.addWaypoint(location);
			}

			// save only after destination is confirmed TODO: transaction
			long eID = mDB.insertExpedition(mExpedition);
			mRoute.setEID(eID);
			mDB.insertRoute(mRoute);
			Leg.a(mRoute.toString());

			startMarker.clearOverlay();
			startMarker.addLocation(
					location,
					"Start",
					"Reach the destination and claim your prize.");
			destMarker.clearOverlay();
			destMarker.addLocation(
					// mExpedition.getDestLoc(),
					mRoute.getNextWaypoint(),
					"Destination",
					"There seems to be something shimmering over there!");

			// center the map
			mController.setCenter(new GeoPoint(
					(int) (location.getLatitude() * 1E6),
					(int) (location.getLongitude() * 1E6)));
			mController.setZoom(14);

			isCalibrated = true;

			if (pdLock.isShowing()) {
				pdLock.dismiss();
			}
		}
		map.invalidate();

		// log traveled distance
		if (mExpedition.getLKLocation() != null) {
			double travelled = mExpedition.getLKLocation().distanceTo(location);
			mExpedition.incTravelled(travelled);
		}

		mExpedition.setLKLocation(location);

		if (isAtDestination(location)) {
			mExpedition.setPrize(Treasure.generateReward(mExpedition));
			concludeExpedition();
			showDialog(EXPEDITION_COMPLETE_DIALOG);
		}

		// debug info
		Leg.a("expedition=" + mExpedition.toString());
	}

	public void onProviderDisabled(String provider) {
	}

	public void onProviderEnabled(String provider) {

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// Echo.a(this, "provider=" + provider + " status=" + status + " extras="
		// + extras.toString());
	}

	/***********************************/
	/** Interface SensorEventListener **/
	/***********************************/

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	public void onSensorChanged(SensorEvent event) {
		int type = event.sensor.getType();
		if (type == Sensor.TYPE_ACCELEROMETER) {
			System.arraycopy(event.values, 0, mAcceler, 0, 3);
		} else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
			System.arraycopy(event.values, 0, mMagnetic, 0, 3);
		}

		float[] r = new float[9], bearing = new float[3];
		if (SensorManager.getRotationMatrix(r, null, mAcceler, mMagnetic)) {
			SensorManager.getOrientation(r, bearing);
			// tvBearing.setText(String.format("x=%.5f y=%.5f z=%.5f",
			// bearing[0],
			// bearing[1], bearing[2]));
		}
	}

	/****************************/
	/** Base class MapActivity **/
	/****************************/

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	/**************/
	/** Privates **/
	/**************/

	private boolean isAtDestination(Location location) {
		// log remaining distance
		// double distLeft = location.distanceTo(mExpedition.getDestLoc());
		double distLeft = location.distanceTo(mRoute.getNextWaypoint());
		mExpedition.setDistanceLeft(distLeft);

		// if the player is close enough to the waypoint
		if (distLeft < mExpedition.getDistance() * 0.05 || distLeft < 10D) {
			Leg.a("apart=" + distLeft + " required="
					+ mExpedition.getDistance() * 0.05);
			mRoute.isAtWaypoint();

			// if there are no more waypoints to go
			if (mRoute.getNextWaypoint() == null) {
				return true;
			} else {
				destMarker.clearOverlay();
				destMarker.addLocation(
						mRoute.getNextWaypoint(),
						"Destination",
						"There seems to be something shimmering over there!");

				// clear start marker since they overlap
				if (mExpedition.getMode() == Const.MODE_TOANDFRO) {
					startMarker.clearOverlay();
				}
				map.invalidate();
			}
		}
		return false;
	}

	private void concludeExpedition() {
		mExpedition.setEndTS(System.currentTimeMillis());
		mSP.setActiveExpeditionID(0);

		mLM.removeUpdates(this);
		mSM.unregisterListener(this);

		isAtDest = true;
	}
}
