package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.R;
import nctuw.littledot.util.Leg;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class TestBundleActivity extends Activity {
	private TextView	tvDebug;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.bundletest_act_layout);
		tvDebug = (TextView) findViewById(R.id.bundle_tv_debug);

		Bundle bundle = getIntent().getExtras();
		Leg.a(bundle.getParcelable(ExpeditionActivity.BUND_EXPEDITION).toString());
	}
}
