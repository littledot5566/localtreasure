package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.Const;
import nctuw.littledot.localtreasure.R;
import nctuw.littledot.localtreasure.database.Expedition;
import nctuw.littledot.util.Leg;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.Toast;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;

public class HistoryActivity extends Activity {

	private TextView tvExcuse;
	private ListView lvHistory;
	private HistoryActivityAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.history_act_layout);
		tvExcuse = (TextView) findViewById(R.id.history_tv_empty);
		lvHistory = (ListView) findViewById(R.id.history_lv_expeditions);

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setTitle("Journal");

		mAdapter = new HistoryActivityAdapter(this);
		mAdapter.registerDataSetObserver(new DataSetObserver() {
			@Override
			public void onChanged() {
				super.onChanged();
				checkEmpty();
			}
		});
		lvHistory.setAdapter(mAdapter);
		lvHistory.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Leg.a("arg2=" + arg2 + " arg3=" + arg3);
				// deleteExpeditionDialog(arg2).show();
				Bundle args = new Bundle();
				args.putInt(KEY_POS, arg2);
				showDialog(DIALOG_EXP_OPTIONS, args);
			}
		});

		checkEmpty();
	}

	private void checkEmpty() {
		if (mAdapter.getCount() == 0)
			tvExcuse.setVisibility(View.VISIBLE);
		else
			tvExcuse.setVisibility(View.GONE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	private int DIALOG_EXP_OPTIONS = 1;
	private int DIALOG_CONFIRM = 2;
	private String KEY_POS = "pos";

	@Override
	@Deprecated
	protected android.app.Dialog onCreateDialog(int id, final Bundle args) {

		if (id == DIALOG_EXP_OPTIONS) {
			final int pos = args.getInt(KEY_POS);

			AlertDialog.Builder b = new AlertDialog.Builder(this);
			b.setItems(new CharSequence[] { "Continue", "Delete" },
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Leg.a("which=" + which);
							if (which == 0) {
								ContentValues expedition = mAdapter.getItem(pos);
								// expedition has not concluded yet
								if (expedition.getAsLong(Expedition.KEY_END_TIMESTAMP) == 0) {

									Intent intent = new Intent(HistoryActivity.this,
											ExpeditionActivity.class);
									intent.putExtra(Const.BUNDLE_SOURCE, HistoryActivity.this
											.getClass().getName());
									intent.putExtra(Const.BUNDLE_EID, mAdapter.getItemId(pos));
									startActivity(intent);

									finish();
								} else
									Toast.makeText(HistoryActivity.this,
											"This expedition has concluded and cannot be continued.",
											Toast.LENGTH_SHORT).show();
							} else if (which == 1) {
								showDialog(DIALOG_CONFIRM, args);
								// mAdapter.deleteExpedition(pos);
							}

							dialog.dismiss();
						}
					});
			return b.create();
			
		} else if (id == DIALOG_CONFIRM) {
			AlertDialog.Builder b = new AlertDialog.Builder(
					HistoryActivity.this);

			final int pos = args.getInt(KEY_POS);

			b.setTitle("Delete expedition?")
					.setMessage("Are you sure you want to delete this record?")
					.setPositiveButton("Yes", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mAdapter.deleteExpedition(pos);
							dialog.dismiss();
						}
					}).setNegativeButton("No", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			return b.create();
		}

		return super.onCreateDialog(id, args);
	}

	// public Dialog deleteExpeditionDialog(final int pos) {
	// AlertDialog.Builder b = new AlertDialog.Builder(this);
	// b.setItems(new CharSequence[] { "Continue", "Delete" },
	// new OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// Leg.a("which=" + which);
	// if (which == 0) {
	// ContentValues expedition = mAdapter.getItem(pos);
	// // expedition has not concluded yet
	// if (expedition.getAsLong(Expedition.KEY_END_TIMESTAMP) == 0) {
	//
	// Intent intent = new Intent(HistoryActivity.this,
	// ExpeditionActivity.class);
	// intent.putExtra(Const.BUNDLE_SOURCE, HistoryActivity.this
	// .getClass().getName());
	// intent.putExtra(Const.BUNDLE_EID, mAdapter.getItemId(pos));
	// startActivity(intent);
	//
	// finish();
	// } else
	// Toast.makeText(HistoryActivity.this,
	// "This expedition has concluded and cannot be continued.",
	// Toast.LENGTH_SHORT).show();
	// } else if (which == 1) {
	//
	// AlertDialog.Builder builder = new AlertDialog.Builder(
	// HistoryActivity.this);
	//
	// builder.setTitle("Delete expedition?")
	// .setMessage("Are you sure you want to delete this record?")
	// .setPositiveButton("Yes", new OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// mAdapter.deleteExpedition(pos);
	// dialog.dismiss();
	// }
	// }).setNegativeButton("No", new OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// dialog.dismiss();
	// }
	// });
	//
	// builder.create().show();
	//
	// // mAdapter.deleteExpedition(pos);
	// }
	//
	// dialog.dismiss();
	// }
	// });
	// return b.create();
	// }
}
