package nctuw.littledot.localtreasure.components;

import java.util.ArrayList;

import nctuw.littledot.localtreasure.R;
import nctuw.littledot.localtreasure.database.DB;
import nctuw.littledot.localtreasure.database.Expedition;
import nctuw.littledot.localtreasure.database.Treasure;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TreasureActivityAdapter extends BaseAdapter {
	private Context mCtx;
	private DB mDB;
	private ArrayList<ContentValues> mData = new ArrayList<ContentValues>();

	public TreasureActivityAdapter(Context ctx) {
		mCtx = ctx;
		mDB = new DB(ctx).open(DB.MODE_READ);

		Cursor cur = mDB.rawQuery(String
				.format("SELECT %s, COUNT(*) FROM %s WHERE %s>0 GROUP BY %s",
						Expedition.KEY_TREASURE_ID,
						Expedition.TABLE_NAME,
						Expedition.KEY_TREASURE_ID,
						Expedition.KEY_TREASURE_ID));

		if (cur.moveToFirst()) {
			while (!cur.isAfterLast()) {
				ContentValues kv = new ContentValues();
				kv.put(Expedition.KEY_TREASURE_ID,
						cur.getInt(cur.getColumnIndex(Expedition.KEY_TREASURE_ID)));
				kv.put("COUNT(*)", cur.getInt(cur.getColumnIndex("COUNT(*)")));
				mData.add(kv);
				cur.moveToNext();
			}
		}

		cur.close();
		mDB.close();
	}

	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public ContentValues getItem(int arg0) {
		return mData.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mCtx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.treasure_item, parent, false);
		}

		ImageView ivTreasure = (ImageView) view.findViewById(R.id.tItem_iv_image);
		TextView tvName = (TextView) view.findViewById(R.id.tItem_tv_name);
		TextView tvNum = (TextView) view.findViewById(R.id.tItem_tv_amount);

		ContentValues kv = mData.get(position);
		Treasure t = Treasure.getTreasure(kv
				.getAsInteger(Expedition.KEY_TREASURE_ID));
		ivTreasure.setImageResource(t.getDrawableID());
		tvName.setText(t.getName());
		tvNum.setText("" + kv.getAsInteger("COUNT(*)"));

		return view;
	}
}
