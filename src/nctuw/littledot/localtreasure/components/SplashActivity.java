package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.R;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.preference.PreferenceManager;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SplashActivity extends Activity {
	public static int SPLASH_PAUSE = 1000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Sherlock_NoActionBar);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.splash_act_layout);
		TextView logo = (TextView) findViewById(R.id.splash_tv_name);

		logo.postDelayed(new Runnable() {
			public void run() {
				Intent mainActivity = new Intent(SplashActivity.this,
						MainActivity.class);
				startActivity(mainActivity);
				finish();
			}
		}, SPLASH_PAUSE);

		PreferenceManager.setDefaultValues(this, R.xml.settings_act, false);

	}
}
