package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.Const;
import nctuw.littledot.localtreasure.R;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;

public class ExpeditionConfigActivity extends Activity {
	private InputMethodManager imm;

	private CheckBox cbOneWay;
	private CheckBox cbToFro;

	private int mode = Const.MODE_ONEWAY;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.expeditionconfig_act);
		cbOneWay = (CheckBox) findViewById(R.id.config_cb_oneway);
		cbToFro = (CheckBox) findViewById(R.id.config_cb_toandfro);

		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setTitle("Set your distance");

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {
		int id = v.getId();

		if (id == R.id.config_cb_oneway) {
			cbOneWay.setChecked(true);
			cbToFro.setChecked(false);
			mode = Const.MODE_ONEWAY;
		} else if (id == R.id.config_cb_toandfro) {
			cbOneWay.setChecked(false);
			cbToFro.setChecked(true);
			mode = Const.MODE_TOANDFRO;
		}
	}

	public void onChooseDistance(View v) {
		int id = v.getId();

		if (id == R.id.distance_but_500m) {
			startExpedition(500D);
		} else if (id == R.id.distance_but_1000m) {
			startExpedition(1000D);
		}
	}

	public void onChooseCustomDistance(View v) {
		EditText et = (EditText) findViewById(R.id.distance_et_custom);

		try {
			double d = Double.parseDouble(et.getText().toString());

			// cannot > 1/2 * circumference of Earth (40075km)
			if (d > 20000000) {
				d = 20000000;
				et.setText("" + d);
				Toast.makeText(this, "Custom distance must < 20,000km",
						Toast.LENGTH_LONG).show();

			} else {
				imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
				startExpedition(d);

			}
		} catch (NumberFormatException e) {
			Toast.makeText(this, "Please enter a valid number.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void startExpedition(double distance) {
		Intent intent = new Intent(this, ExpeditionActivity.class);
		intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
		intent.putExtra(Const.BUNDLE_DISTANCE, distance);
		intent.putExtra(Const.BUNDLE_MODE, mode);
		startActivity(intent);
		finish();
	}
}
