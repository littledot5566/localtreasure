package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.R;

import org.holoeverywhere.app.Activity;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class CreditsActivity extends Activity {

	private TextView tvThx;
	private TextView tvIssues;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.credits_act);
		tvThx = (TextView) findViewById(R.id.credits_tv_thx);
		tvIssues = (TextView) findViewById(R.id.credits_tv_issues);
		tvThx.setMovementMethod(LinkMovementMethod.getInstance());
		tvIssues.setMovementMethod(LinkMovementMethod.getInstance());

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setTitle("Credits");

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
