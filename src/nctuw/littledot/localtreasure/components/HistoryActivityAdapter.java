package nctuw.littledot.localtreasure.components;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;

import nctuw.littledot.localtreasure.R;
import nctuw.littledot.localtreasure.database.DB;
import nctuw.littledot.localtreasure.database.Expedition;
import nctuw.littledot.localtreasure.database.Profile;
import nctuw.littledot.localtreasure.database.SP;
import nctuw.littledot.localtreasure.database.Treasure;
import nctuw.littledot.util.Leg;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HistoryActivityAdapter extends BaseAdapter {
	private Context mCtx;
	private DB mDB;
	private SP mSP;

	private ArrayList<ContentValues> mData = new ArrayList<ContentValues>();

	public HistoryActivityAdapter(Context ctx) {
		mCtx = ctx;
		mDB = new DB(ctx).open(DB.MODE_READ);
		mSP = new SP(ctx);

		Cursor cur = mDB.rawQuery(String.format(
				"SELECT * FROM %s, %s WHERE %s.%s=%s.%s AND %s=1 ORDER BY %s",
				Expedition.TABLE_NAME, Profile.TABLE_NAME, // tables
				Expedition.TABLE_NAME, Expedition.KEY_PID, // join
				Profile.TABLE_NAME, Profile.KEY_PID, // on
				Expedition.KEY_DISPLAY, // where
				Expedition.KEY_START_TIMESTAMP // order by
				));

		if (cur.moveToFirst()) {
			while (!cur.isAfterLast()) {
				Leg.a(DatabaseUtils.dumpCurrentRowToString(cur));
				ContentValues kv = Expedition.toContentValues(cur);
				kv.putAll(Profile.toContentValues(cur));
				mData.add(kv);

				cur.moveToNext();
			}
		}
		cur.close();
		mDB.close();
	}

	public void deleteExpedition(int pos) {
		long id = getItemId(pos);

		mDB.open(DB.MODE_WRITE);

		if (mDB.deleteExpedition(id) == 1) {
			mData.remove(pos);

			if (mSP.getActiveExpeditionID() == id)
				mSP.setActiveExpeditionID(0);
		}

		notifyDataSetChanged();

		mDB.close();
	}

	public int getCount() {
		return mData.size();
	}

	public ContentValues getItem(int position) {
		return mData.get(position);
	}

	public long getItemId(int position) {
		return mData.get(position).getAsLong(Expedition.KEY_EID);
	}

	public View getView(int position, View view, ViewGroup parent) {
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mCtx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.history_item, parent, false);
		}

		long l;
		int i;

		ContentValues kv = mData.get(position);

		TextView tvTravelled = (TextView) view
				.findViewById(R.id.hItem_tv_travelled);
		TextView tvDistance = (TextView) view
				.findViewById(R.id.hItem_tv_distance);
		TextView tvDistLeft = (TextView) view.findViewById(R.id.hItem_tv_distleft);
		TextView tvUser = (TextView) view.findViewById(R.id.hItem_tv_user);
		TextView tvStTime = (TextView) view.findViewById(R.id.hItem_tv_stTime);
		TextView tvEndTime = (TextView) view.findViewById(R.id.hItem_tv_endTime);
		TextView tvTreasure = (TextView) view.findViewById(R.id.hItem_tv_treasure);
		ImageView ivTreasure = (ImageView) view
				.findViewById(R.id.hItem_iv_treasure);

		NumberFormat nf = NumberFormat.getInstance();

		tvTravelled.setText(nf.format(
				kv.getAsDouble(Expedition.KEY_TRAVELED)) + "m");
		tvDistance.setText(nf.format(
				kv.getAsDouble(Expedition.KEY_DISTANCE)) + "m");
		tvDistLeft.setText(nf.format(
				kv.getAsDouble(Expedition.KEY_DIST_LEFT)) + "m");
		tvUser.setText(kv.getAsString(Profile.KEY_NAME));
		tvStTime.setText(new Date(kv.getAsLong(Expedition.KEY_START_TIMESTAMP))
				.toLocaleString());
		tvEndTime
				.setText((l = kv.getAsLong(Expedition.KEY_END_TIMESTAMP)) > 0
						? new Date(l).toLocaleString()
						: "");

		if ((i = kv.getAsInteger(Expedition.KEY_TREASURE_ID)) > 0) {
			Treasure t = Treasure.getTreasure(i);
			tvTreasure.setText(t.getName());
			tvTreasure.setVisibility(View.VISIBLE);
			ivTreasure.setImageResource(t.getDrawableID());
			ivTreasure.setVisibility(View.VISIBLE);
		} else {
			tvTreasure.setVisibility(View.INVISIBLE);
			ivTreasure.setVisibility(View.INVISIBLE);
		}

		return view;
	}
}
