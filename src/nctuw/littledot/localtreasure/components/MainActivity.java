package nctuw.littledot.localtreasure.components;

import nctuw.littledot.localtreasure.Const;
import nctuw.littledot.localtreasure.R;
import nctuw.littledot.localtreasure.database.DB;
import nctuw.littledot.localtreasure.database.Profile;
import nctuw.littledot.localtreasure.database.SP;
import nctuw.littledot.util.Leg;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;

public class MainActivity extends Activity {
	public static final String BUN_PROFILE = "bunProfile";
	public static final String BUN_EID = "bunEID";
	private TextView tvInfo;

	private DB mDB;
	private SP mSPManager;
	private Profile mProfile;

	private Button butContinue;

	private long mActiveEID = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_act_layout);
		butContinue = (Button) findViewById(R.id.main_but_continue);
		tvInfo = (TextView) findViewById(R.id.main_tv_info);

		mSPManager = new SP(this);
		mDB = new DB(this).open(DB.MODE_WRITE);

		ActionBar ab = getSupportActionBar();
		ab.setTitle("Main Menu");

	}

	@Override
	protected void onResume() {
		super.onResume();
		mDB.open(DB.MODE_WRITE);

		// load active profile
		mProfile = mDB.queryProfile(mSPManager.getActiveProfileID());
		Leg.a(mProfile.toString());

		// check for active expeditions
		mActiveEID = mSPManager.getActiveExpeditionID();
		Leg.a("activeExp=" + mActiveEID);
		if ((mActiveEID = mSPManager.getActiveExpeditionID()) > 0) {
			butContinue.setText("Continue Expedition");
			butContinue.setVisibility(View.VISIBLE);
		} else
			butContinue.setVisibility(View.GONE);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mDB.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		return dialog;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
		super.onPrepareDialog(id, dialog, args);
		Leg.a("");
	}

	public void onClick(View v) {

		int id = v.getId();

		if (id == R.id.main_but_startExpedition) {
			Intent intent = new Intent(this, ExpeditionConfigActivity.class);
			intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
			startActivity(intent);

		} else if (id == R.id.main_but_profile) {
			Intent intent = new Intent(this, ProfileActivity.class);
			intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
			intent.putExtra(Const.BUNDLE_PROFILE, mProfile);
			startActivity(intent);

		} else if (id == R.id.main_but_continue) {
			Intent intent = new Intent(this, ExpeditionActivity.class);
			intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
			intent.putExtra(Const.BUNDLE_EID, mActiveEID);
			startActivity(intent);

		} else if (id == R.id.main_but_history) {
			Intent intent = new Intent(this, HistoryActivity.class);
			intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
			startActivity(intent);

		} else if (id == R.id.main_but_chest) {
			Intent intent = new Intent(this, TreasureActivity.class);
			intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
			startActivity(intent);

		} else if (id == R.id.main_but_settings) {
			Intent intent = new Intent(this, SettingsActivity.class);
			intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
			startActivity(intent);

		} else if (id == R.id.main_but_credits) {
			Intent intent = new Intent(this, CreditsActivity.class);
			intent.putExtra(Const.BUNDLE_SOURCE, getClass().getName());
			startActivity(intent);

		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Handle the back button
		if (mSPManager.getExitDialogSettings()
				&& keyCode == KeyEvent.KEYCODE_BACK && isTaskRoot()) {
			// Ask the user if they want to quit
			new AlertDialog.Builder(this)
					.setTitle("Goodbye  ( ´ ▽ ` )ﾉ")
					.setMessage("Would you like to exit the application?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// Stop the activity
									finish();
								}
							})
					.setNegativeButton("no", null)
					.show();

			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
