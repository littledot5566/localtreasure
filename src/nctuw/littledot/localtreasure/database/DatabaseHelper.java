package nctuw.littledot.localtreasure.database;

import nctuw.littledot.util.Curser;
import nctuw.littledot.util.Leg;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	public DatabaseHelper(Context context, int version) {
		super(context, context.getPackageName(), null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		for (Patch patch : patches) {
			Leg.a("patch v=" + patch.version);
			patch.apply(db);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Leg.a("old=" + oldVersion + " new=" + newVersion + " patches="
				+ patches.length);

		db.beginTransaction();
		try {
			for (Patch patch : patches) {
				Leg.a("patch v=" + patch.version);
				if (patch.version > oldVersion) {
					patch.apply(db);
				}
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private static Patch[] patches = {
			new Patch(6) {
				/*
				 * Patch 6: New Profile & Expedition tables.
				 */
				@Override
				public void apply(SQLiteDatabase db) {
					db.execSQL(Profile.SQL_CREATE_PROFILE);
					db.execSQL(Expedition.SQL_CREATE_EXPEDITION);
					db.execSQL("insert into " + Profile.TABLE_NAME + "("
							+ Profile.KEY_PID + ") values (1);");
				}

				@Override
				public void revert(SQLiteDatabase db) {
				}

			},
			new Patch(7) {
				/*
				 * Patch 7: New Route table. Expeditions now store a list of waypoints.
				 */
				@Override
				public void apply(SQLiteDatabase db) {
					db.execSQL(Route.SQL_CREATE_ROUTE);

					String sql = "select * from " + Expedition.TABLE_NAME;
					Cursor c = db.rawQuery(sql, null);
					c.moveToFirst();

					ContentValues kv = new ContentValues();

					while (!c.isAfterLast()) {
						// starting location is waypoint 0
						kv.put(Route.KEY_EID, Curser.getLong(c, Expedition.KEY_EID));
						kv.put(Route.KEY_ORDER, 0);
						kv.put(Route.KEY_VISITED, 1);
						kv.put(Route.KEY_LAT, Curser.getDouble(c, Expedition.KEY_START_LAT));
						kv.put(Route.KEY_LONG,
								Curser.getDouble(c, Expedition.KEY_START_LONG));
						db.insert(Route.TABLE_NAME, null, kv);

						// destination is waypoint 1
						kv.put(Route.KEY_ORDER, 1);
						kv.put(Route.KEY_LAT, Curser.getDouble(c, Expedition.KEY_END_LAT));
						kv.put(Route.KEY_LONG, Curser.getDouble(c, Expedition.KEY_END_LONG));
						if (Curser.getLong(c, Expedition.KEY_TIME) > 0)
							kv.put(Route.KEY_VISITED, 1);
						else
							kv.put(Route.KEY_VISITED, 0);
						db.insert(Route.TABLE_NAME, null, kv);

						kv.clear();
						c.moveToNext();
					}
				}

				@Override
				public void revert(SQLiteDatabase db) {
				}
			}
	};
}