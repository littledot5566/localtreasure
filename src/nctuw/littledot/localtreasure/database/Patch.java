package nctuw.littledot.localtreasure.database;

import android.database.sqlite.SQLiteDatabase;

public abstract class Patch {
	final int version;

	public Patch(int v) {
		version = v;
	}

	abstract void apply(SQLiteDatabase db);

	abstract void revert(SQLiteDatabase db);
}