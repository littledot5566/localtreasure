package nctuw.littledot.localtreasure.database;

import java.util.ArrayList;

import nctuw.littledot.util.Leg;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DB {
	private static final int DB_VERSION = 7;
	public static final String DBNAME = "nctuw.littledot.localtreasure";

	public static final int MODE_READ = 1;
	public static final int MODE_WRITE = 2;

	private final Context mContext;

	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public DB(Context ctx) {
		this.mContext = ctx;
		DBHelper = new DatabaseHelper(mContext, DB_VERSION);
	}

	public DB open(int mode) {
		if (mode == MODE_WRITE)
			db = DBHelper.getWritableDatabase();
		else if (mode == MODE_READ)
			db = DBHelper.getReadableDatabase();
		else
			throw new IllegalArgumentException("Unknown access argument");
		return this;
	}

	public void close() {
		if (db.isOpen())
			db.close();
	}

	public int getVersion() {
		return db.getVersion();
	}

	public void showAllData(String table) {
		Cursor cur = db.query(table, null, null, null, null, null, null);
		if (cur != null)
			cur.moveToFirst();

		while (!cur.isAfterLast()) {
			String log = "";
			for (int i = 4; i < 9; i++) {
				log += "[" + i + "]=";
				log += cur.getInt(i);
			}
			Leg.i(log);
			cur.moveToNext();
		}
	}

	/*************/
	/** Queries **/
	/*************/

	public Cursor rawQuery(String sql) {
		return db.rawQuery(sql, null);
	}

	public Profile queryProfile(long profileID) {
		Cursor cur = rawQuery(String
				.format(
						"SELECT *, %s, %s FROM %s LEFT JOIN %s ON %s.%s=%s.%s WHERE %s=%d GROUP BY %s",
						Expedition.SUM_TRAVELED, Expedition.COUNT_ID,
						Profile.TABLE_NAME, Expedition.TABLE_NAME,
						Profile.TABLE_NAME, Profile.KEY_PID,
						Expedition.TABLE_NAME,
						Expedition.KEY_PID,
						Profile.KEY_PID, profileID,
						Profile.KEY_PID
				));

		Leg.dumpCursor(cur);

		if (cur.moveToFirst()) {
			Profile profile = new Profile(cur);
			cur.close();

			return profile;
		} else
			return null;
	}

	public Expedition queryExpedition(long eID) {
		String query = new StringBuilder("select * from ")
				.append(Expedition.TABLE_NAME).append(" where ")
				.append(Expedition.KEY_EID).append("=").append(eID).toString();

		Cursor cur = rawQuery(query);

		if (cur.moveToFirst()) {
			Expedition e = new Expedition(cur);
			cur.close();
			return e;
		} else
			return null;
	}

	public ArrayList<Expedition> queryExpeditions(String where) {
		String query = "select * from " + Expedition.TABLE_NAME + ' ';
		if (!where.equals("*"))
			query += where;

		Cursor c = rawQuery(query);

		if (c.moveToFirst()) {
			ArrayList<Expedition> expeditions = new ArrayList<Expedition>();

			while (!c.isAfterLast()) {
				expeditions.add(new Expedition(c));
				c.moveToNext();
			}
			c.close();

			return expeditions;
		} else
			return null;
	}

	public Route queryRoute(long eID) {
		String sql = "select * from " + Route.TABLE_NAME + " where "
				+ Route.KEY_EID + "=" + eID + " order by " + Route.KEY_ORDER + " asc";
		Cursor c = rawQuery(sql);

		Leg.dumpCursor(c);

		if (c.moveToFirst()) {

			Route r = new Route(c);
			Leg.a(r.toString());
			return r;
		}
		else
			return null;
	}

	/*************/
	/** Inserts **/
	/*************/

	public long insert(String table, String[] keys, Object[] values) {
		// keys and values must match
		if (keys.length != values.length)
			throw new IllegalArgumentException(
					"keys-values must be of the same length");

		ContentValues kvPair = toContentValues(keys, values);
		long result = db.insert(table, null, kvPair);
		return result;
	}

	public long insertExpedition(Expedition expedition) {
		long eID = db.insertOrThrow(Expedition.TABLE_NAME, null,
				expedition.toContentValues());

		expedition.setEID(eID);

		return eID;
	}

	public long insertProfile(Profile profile) {
		long pID = db.insertOrThrow(Profile.TABLE_NAME, null,
				profile.toContentValues());

		profile.setProfileID(pID);

		return pID;
	}

	public long insertRoute(Route route) {

		ArrayList<ContentValues> wp = route.toContentValues();

		for (ContentValues kv : wp) {
			db.insertOrThrow(Route.TABLE_NAME, null, kv);
		}

		return wp.size();
	}

	/*************/
	/** Updates **/
	/*************/

	public int update(String table, String[] keys, Object[] values,
			String whereClause) {
		// keys and values must match
		if (keys.length != values.length)
			throw new IllegalArgumentException(
					"keys-values must be of the same length");

		ContentValues kvPair = toContentValues(keys, values);

		int result = db.update(table, kvPair, whereClause, null);

		return result;
	}

	/**
	 * Single column version of
	 * {@link #update(String, String[], Object[], String)}.
	 * 
	 * @param table
	 * @param key
	 * @param value
	 * @param whereClause
	 * @return
	 */
	public int update(String table, String key, Object value,
			String whereClause) {
		return update(table, new String[] { key }, new Object[] { value },
				whereClause);
	}

	/**
	 * Flushes an {@link Expedition} to database.
	 * 
	 * @param expedition
	 * @return
	 */
	public int updateExpedition(Expedition expedition) {
		// WHERE eID = %l
		String where = new StringBuilder(Expedition.KEY_EID)
				.append("=").append(expedition.getEID()).toString();

		return db.update(Expedition.TABLE_NAME,
				expedition.toContentValues(), where, null);
	}

	/**
	 * Flushes a {@link Profile} to database.
	 * 
	 * @param profile
	 * @return
	 */
	public int updateProfile(Profile profile) {
		// WHERE pID = %l
		String where = new StringBuilder(Profile.KEY_PID)
				.append("=").append(profile.getProfileID()).toString();

		return db.update(Profile.TABLE_NAME, profile.toContentValues(), where,
				null);
	}

	/*************/
	/** Deletes **/
	/*************/

	public int delete(String table, String whereClaus, String[] whereArgs) {
		int result = db.delete(table, whereClaus, whereArgs);

		return result;
	}

	public int delete(String table, String whereClaus) {
		return delete(table, whereClaus, null);
	}

	public int deleteExpedition(Expedition exp) {
		return deleteExpedition(exp.getEID());
	}

	public int deleteExpedition(long id) {
		// return delete(Expedition.TABLE_NAME, Expedition.KEY_ID + "=" + id);
		return update(Expedition.TABLE_NAME, Expedition.KEY_DISPLAY, 0,
				Expedition.KEY_EID + "=" + id);
	}

	/***************/
	/** Utilities **/
	/***************/

	public static String intArrToString(int[] intArray) {
		StringBuilder retStr = new StringBuilder();
		for (int i : intArray)
			retStr.append(i);
		return retStr.toString();
	}

	public static int[] stringToIntArr(String str) {
		int len = str.length();
		int[] retArr = new int[len];

		for (int i = 0; i < len; i++)
			retArr[i] = Integer.parseInt("" + str.charAt(i));
		return retArr;
	}

	public static ContentValues toContentValues(String[] keys, Object[] values) {
		if (keys.length != values.length)
			throw new IllegalArgumentException(
					"The number of keys("
							+ keys.length
							+ ") is not equal to the number of values("
							+ values.length + ").");

		ContentValues kv = new ContentValues();

		for (int i = 0; i < keys.length; i++) {
			if (values[i] instanceof String)
				kv.put(keys[i], (String) values[i]);
			else if (values[i] instanceof Integer)
				kv.put(keys[i], (Integer) values[i]);
			else if (values[i] instanceof Double)
				kv.put(keys[i], (Double) values[i]);
			else if (values[i] instanceof Float)
				kv.put(keys[i], (Float) values[i]);
			else if (values[i] instanceof Long)
				kv.put(keys[i], (Long) values[i]);
			else if (values[i] instanceof Short)
				kv.put(keys[i], (Short) values[i]);
			else if (values[i] instanceof Boolean)
				kv.put(keys[i], (Boolean) values[i]);
			else if (values[i] instanceof Byte)
				kv.put(keys[i], (Byte) values[i]);
			else if (values[i] instanceof byte[])
				kv.put(keys[i], (byte[]) values[i]);
			else
				throw new IllegalArgumentException("kv[" + i
						+ "]'s type is not supported.");
		}

		return kv;
	}

	/**
	 * Checks if the value is null before returning the column index.
	 * 
	 * @param c
	 * @param s
	 * @return
	 */
	public static int getIndex(Cursor c, String s) {
		// column index starts at 0
		int i = c.getColumnIndex(s);
		if (c.isNull(i))
			return -1;
		else
			return i;
	}

}
