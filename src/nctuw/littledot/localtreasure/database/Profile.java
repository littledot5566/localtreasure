package nctuw.littledot.localtreasure.database;

import java.util.ArrayList;

import nctuw.littledot.util.Curser;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

public class Profile implements Parcelable {
	private long mProfileID = 1;
	private String mName = "<John Doe>";
	private long mExpeditionCount;
	private double mDistanceTravelled;

	public Profile() {
	}

	public Profile(int id, String name, long expeditions, double distance) {
		mProfileID = id;
		mName = name;
		mExpeditionCount = expeditions;
		mDistanceTravelled = distance;
	}

	public String toString() {
		return String.format("pID=%d name=%s expeditions=%d distance=%f",
				mProfileID, mName, mExpeditionCount, mDistanceTravelled);
	}

	public long getProfileID() {
		return mProfileID;
	}

	public void setProfileID(long mProfileID) {
		this.mProfileID = mProfileID;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public long getTotalExpeditions() {
		return mExpeditionCount;
	}

	public void setTotalExpeditions(long mTotalExpeditions) {
		this.mExpeditionCount = mTotalExpeditions;
	}

	public double getTotalDistance() {
		return mDistanceTravelled;
	}

	public void setTotalDistance(double mTotalDistance) {
		this.mDistanceTravelled = mTotalDistance;
	}

	/************/
	/** SQLite **/
	/************/

	public static final String TABLE_NAME = "profile";
	public static final String KEY_PID = "pID";
	public static final String KEY_NAME = "pName";

	public static final String SQL_CREATE_PROFILE = "create table if not exists "
			+ TABLE_NAME
			+ "("
			+ KEY_PID
			+ " integer not null primary key autoincrement,"
			+ KEY_NAME
			+ " text default '<John Doe>'"
			+ ");";

	public Profile(Cursor c) {
		mProfileID = Curser.getLong(c, KEY_PID);
		mName = Curser.getString(c, KEY_NAME);
		mDistanceTravelled = Curser.getDouble(c, Expedition.SUM_TRAVELED);
		mExpeditionCount = Curser.getInt(c, Expedition.COUNT_ID);
	}

	public ContentValues toContentValues() {
		ArrayList<String> keys = new ArrayList<String>();
		ArrayList<Object> vals = new ArrayList<Object>();

		keys.add(KEY_PID);
		vals.add(mProfileID);

		keys.add(KEY_NAME);
		vals.add(mName);

		return DB.toContentValues(
				(String[]) keys.toArray(new String[keys.size()]),
				vals.toArray());
	}

	public static ContentValues toContentValues(Cursor c) {
		ContentValues kv = new ContentValues();

		kv.put(KEY_PID, Curser.getLong(c, KEY_PID));
		kv.put(KEY_NAME, Curser.getString(c, KEY_NAME));

		return kv;
	}

	/**************************/
	/** Interface Parcelable **/
	/**************************/

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(mProfileID);
		dest.writeString(mName);
		dest.writeLong(mExpeditionCount);
		dest.writeDouble(mDistanceTravelled);
	}

	public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
		public Profile createFromParcel(
				Parcel in) {
			return new Profile(
					in);
		}

		public Profile[] newArray(
				int size) {
			return new Profile[size];
		}
	};

	private Profile(Parcel in) {
		mProfileID = in.readLong();
		mName = in.readString();
		mExpeditionCount = in.readLong();
		mDistanceTravelled = in.readDouble();
	}
}
