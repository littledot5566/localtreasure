package nctuw.littledot.localtreasure.database;

import nctuw.littledot.localtreasure.Const;
import nctuw.littledot.util.Leg;

import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;

import android.content.Context;

public class SP {
	public static final String DBTABLE_STATS = "statistics";
	public static final String KEY_STATS_ID = "sID";
	public static final String KEY_STATS_DIST_TRAV = "sDistanceTraveled";
	public static final String KEY_STATS_EXP_TOTAL = "sTotalExpeditionsLaunched";
	public static final String KEY_STATS_EXP_SUCC = "sSuccessfulExpeditions";

	public static final String KEY_ACTIVE_PROFILE_ID = "spActiveProfileID";
	public static final String KEY_ACTIVE_EXPEDITION_ID = "spActiveExpeditionID";

	private Context mCtx;
	private SharedPreferences mPrefs;
	private SharedPreferences.Editor mEditor;

	public SP(Context ctx) {
		mCtx = ctx;
		// mPrefs = ctx.getSharedPreferences(Const.PACKAGE,
		// Context.MODE_PRIVATE);
		mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		mEditor = mPrefs.edit();
	}

	public long getActiveProfileID() {
		return mPrefs.getLong(KEY_ACTIVE_PROFILE_ID, 1);
	}

	public void setActiveProfileID(long id) {
		mEditor.putLong(KEY_ACTIVE_PROFILE_ID, id);
		mEditor.commit();
	}

	public long getActiveExpeditionID() {
		long l = mPrefs.getLong(KEY_ACTIVE_EXPEDITION_ID, 0);
		Leg.a("KEY_ACTIVE_EXPEDITION_ID=" + l);
		return l;
	}

	public void setActiveExpeditionID(long id) {
		Leg.a("KEY_ACTIVE_EXPEDITION_ID=" + id);
		mEditor.putLong(KEY_ACTIVE_EXPEDITION_ID, id);
		mEditor.commit();
	}

	/**************/
	/** Settings **/
	/**************/

	public boolean getExitDialogSettings() {
		return mPrefs.getBoolean(Const.SETTINGS_EXIT_DIALOG, true);
	}

}
