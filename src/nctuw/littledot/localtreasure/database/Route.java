package nctuw.littledot.localtreasure.database;

import java.util.ArrayList;

import nctuw.littledot.util.Curser;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;

public class Route {
	private long eID = 0;
	private ArrayList<Location> mWaypoints = new ArrayList<Location>();

	private int mAt = 0;

	public Route() {
	}

	public String toString() {
		String s = "eID=" + eID + " at=" + mAt + " size=" + mWaypoints.size();

		s += " waypoints=";
		for (Location l : mWaypoints) {
			s += "(" + l.getLongitude() + "," + l.getLatitude() + ")";
		}

		return s;
	}

	public long getEID() {
		return eID;
	}

	public Route setEID(long eID) {
		this.eID = eID;
		return this;
	}

	public Route addWaypoint(Location l) {
		mWaypoints.add(l);
		return this;
	}

	public void isAtWaypoint() {
		mAt++;
	}

	public Location getNextWaypoint() {
		if (mAt + 1 < mWaypoints.size())
			return new Location(mWaypoints.get(mAt + 1));
		else
			return null;
	}

	public Location getWaypoint(int i) {
		return new Location(mWaypoints.get(i));
	}

	public int size() {
		return mWaypoints.size();
	}

	/************/
	/** SQLite **/
	/************/

	public static final String TABLE_NAME = "route";
	public static final String KEY_RID = "rID";
	public static final String KEY_EID = "eID";
	public static final String KEY_ORDER = "rOrder";
	public static final String KEY_VISITED = "rVisited";
	public static final String KEY_LAT = "rLat";
	public static final String KEY_LONG = "rLong";

	public static final String SQL_CREATE_ROUTE = "create table if not exists  "
			+ TABLE_NAME
			+ "("
			+ KEY_RID
			+ " integer not null primary key autoincrement,"
			+ KEY_EID
			+ " integer not null,"
			+ KEY_ORDER
			+ " integer not null,"
			+ KEY_VISITED
			+ " integer not null,"
			+ KEY_LAT
			+ " real not null,"
			+ KEY_LONG
			+ " real not null"
			+ ");";

	public Route(Cursor c) {
		c.moveToFirst();

		eID = Curser.getLong(c, KEY_EID);

		int order = 0;
		boolean isAtSet = false;

		while (!c.isAfterLast()) {
			if (!isAtSet && Curser.getInt(c, KEY_VISITED) == 0) {
				mAt = order - 1;
				isAtSet = true;
			}

			if (order != Curser.getInt(c, KEY_ORDER))
				throw new IllegalStateException("Data is not in order.");
			order++;

			Location l = new Location("");
			l.setLatitude(Curser.getDouble(c, KEY_LAT));
			l.setLongitude(Curser.getDouble(c, KEY_LONG));
			mWaypoints.add(l);

			c.moveToNext();
		}
	}

	public ArrayList<ContentValues> toContentValues() {
		ArrayList<ContentValues> cv = new ArrayList<ContentValues>();

		ArrayList<String> keys = new ArrayList<String>();
		ArrayList<Object> vals = new ArrayList<Object>();
		for (int i = 0; i < mWaypoints.size(); i++) {
			keys.clear();
			vals.clear();

			keys.add(KEY_EID);
			vals.add(eID);

			keys.add(KEY_ORDER);
			vals.add(i);

			keys.add(KEY_LAT);
			vals.add(mWaypoints.get(i).getLatitude());

			keys.add(KEY_LONG);
			vals.add(mWaypoints.get(i).getLongitude());

			keys.add(KEY_VISITED);
			if (i <= mAt)
				vals.add(1);
			else
				vals.add(0);

			cv.add(DB.toContentValues(
					(String[]) keys.toArray(new String[keys.size()]),
					vals.toArray()));
		}

		return cv;
	}
}
