package nctuw.littledot.localtreasure.database;

import java.util.ArrayList;
import java.util.Date;

import nctuw.littledot.util.Curser;
import nctuw.littledot.util.Leg;
import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class Expedition implements Parcelable {
	private long eID = 0;

	private long pID = 0;
	// private Location mStartLoc = null;
	// private Location mDestLoc = null;
	private double mDistance;
	private double mTravelled;
	private double mDistanceLeft;

	private int mode;
	// private Route mRoute;

	private long mStartTS;
	private long mEndTS;

	private Treasure mPrize = null;

	private Location mLKLocation = null;

	public Expedition() {
	}

	public String toString() {
		String sb = "eID=" + eID + " pID=" + pID;

		// if (mStartLoc != null)
		// sb += " stLoc=(" + mStartLoc.getLatitude() + ","
		// + mStartLoc.getLongitude() + ")";
		// if (mDestLoc != null)
		// sb += " destLoc=(" + mDestLoc.getLatitude() + ","
		// + mDestLoc.getLongitude() + ")";

		sb += " dist=" + mDistance
				+ " tra=" + mTravelled
				+ " left=" + mDistanceLeft
				+ " stTS=" + new Date(mStartTS).toString()
				+ " endTS=" + new Date(mEndTS).toString();

		if (mPrize != null)
			sb += " prize=" + mPrize.toString();

		return sb.toString();
	}

	public long getEID() {
		return eID;
	}

	public Expedition setEID(long eID) {
		this.eID = eID;
		// mRoute.setEID(eID);
		return this;
	}

	public long getPID() {
		return pID;
	}

	public Expedition setPID(long pID) {
		this.pID = pID;
		return this;
	}

	// public Location getStartLoc() {
	// return mStartLoc;
	// }
	//
	// public Expedition setStartLoc(Location mStartLoc) {
	// this.mStartLoc = mStartLoc;
	// return this;
	// }
	//
	// public Location getDestLoc() {
	// return mDestLoc;
	// // return mRoute.getNextWaypoint();
	// }
	//
	// public Expedition setDestLoc(Location mDestLoc) {
	// this.mDestLoc = mDestLoc;
	// return this;
	// }

	public double getDistance() {
		return mDistance;
	}

	public Expedition setDistance(double mDistance) {
		this.mDistance = mDistance;
		return this;
	}

	public double getTravelled() {
		return mTravelled;
	}

	public Expedition setTravelled(double mTravelled) {
		this.mTravelled = mTravelled;
		return this;
	}

	public Expedition incTravelled(double mTravelled) {
		this.mTravelled += mTravelled;
		return this;
	}

	public double getDistanceLeft() {
		return mDistanceLeft;
	}

	public Expedition setDistanceLeft(double mDistanceLeft) {
		this.mDistanceLeft = mDistanceLeft;
		return this;
	}

	public long getStartTS() {
		return mStartTS;
	}

	public Expedition setStartTS(long mStartTS) {
		this.mStartTS = mStartTS;
		return this;
	}

	public long getEndTS() {
		return mEndTS;
	}

	public Expedition setEndTS(long mEndTS) {
		this.mEndTS = mEndTS;
		return this;
	}

	public Treasure getPrize() {
		return mPrize;
	}

	public Expedition setPrize(Treasure mPrize) {
		this.mPrize = mPrize;
		return this;
	}

	public Location getLKLocation() {
		return mLKLocation;
	}

	public Expedition setLKLocation(Location mLKLocation) {
		this.mLKLocation = mLKLocation;
		return this;
	}

	public int getMode() {
		return mode;
	}

	public Expedition setMode(int mode) {
		this.mode = mode;
		return this;
	}

	// public Route getRoute() {
	// return mRoute;
	// }
	//
	// public void addWaypoint(Location l) {
	// mRoute.addWaypoint(l);
	// }

	/************/
	/** SQLite **/
	/************/

	public static final String TABLE_NAME = "expedition";
	public static final String KEY_EID = "eID";
	public static final String KEY_PID = "ePID";
	public static final String KEY_DISTANCE = "eDistance";
	public static final String KEY_TRAVELED = "eTraveled";
	/** Remaining distance to destination */
	public static final String KEY_DIST_LEFT = "eDistLeft";
	public static final String KEY_START_LAT = "eStartLat";
	public static final String KEY_START_LONG = "eStartLong";
	public static final String KEY_END_LAT = "eEndLat";
	public static final String KEY_END_LONG = "eEndLong";
	public static final String KEY_START_TIMESTAMP = "eStartTimestamp";
	public static final String KEY_END_TIMESTAMP = "eEndTimestamp";
	public static final String KEY_TIME = "eTime";
	public static final String KEY_TREASURE_ID = "eTreasure";
	public static final String KEY_DISPLAY = "eDisplay";

	public static final String COUNT_ID = "count(" + KEY_EID + ")";
	public static final String SUM_TRAVELED = "sum(" + KEY_TRAVELED + ")";

	public static final String SQL_CREATE_EXPEDITION = "create table if not exists "
			+ TABLE_NAME
			+ "("
			+ KEY_EID // long
			+ " integer not null primary key autoincrement,"
			+ KEY_PID // long
			+ " integer not null,"
			+ KEY_DISTANCE // double
			+ " integer not null,"
			+ KEY_TRAVELED // double
			+ " real default 0,"
			+ KEY_DIST_LEFT
			+ " real,"
			+ KEY_START_LAT // double
			+ " real,"
			+ KEY_START_LONG // double
			+ " real,"
			+ KEY_END_LAT // double
			+ " real,"
			+ KEY_END_LONG // double
			+ " real,"
			+ KEY_START_TIMESTAMP // long
			+ " integer not null,"
			+ KEY_END_TIMESTAMP // long
			+ " integer,"
			+ KEY_TIME // long
			+ " integer,"
			+ KEY_TREASURE_ID // int
			+ " integer,"
			+ KEY_DISPLAY // int 0:hide 1:show
			+ " integer default 1"
			+ ");";

	/**
	 * Populate an Expedition via a Cursor.
	 * 
	 * @param c
	 */
	public Expedition(Cursor c) {
		Leg.dumpCursor(c);
		Location loc;
		int i;

		// setEID(c.getLong(c.getColumnIndex(Expedition.KEY_EID)));
		eID = Curser.getLong(c, Expedition.KEY_EID);
		// setPID(c.getLong(c.getColumnIndex(Expedition.KEY_PID)));
		pID = Curser.getLong(c, Expedition.KEY_PID);
		// setDistance(c.getDouble(c.getColumnIndex(Expedition.KEY_DISTANCE)));
		mDistance = Curser.getDouble(c, Expedition.KEY_DISTANCE);
		// setDistanceLeft(c.getDouble(c.getColumnIndex(KEY_DIST_LEFT)));
		mDistanceLeft = Curser.getDouble(c, Expedition.KEY_DIST_LEFT);

		// if ((i = DB.getIndex(c, Expedition.KEY_TRAVELED)) > -1)
		// setTravelled(c.getDouble(i));
		mTravelled = Curser.getDouble(c, Expedition.KEY_TRAVELED);

		if ((i = DB.getIndex(c, Expedition.KEY_START_LAT)) > -1) {
			loc = new Location("");
			loc.setLatitude(c.getDouble(i));
			loc.setLongitude(c.getDouble(c
					.getColumnIndex(Expedition.KEY_START_LONG)));
			// setStartLoc(loc);
		}

		if ((i = DB.getIndex(c, Expedition.KEY_END_LAT)) > -1) {
			loc = new Location("");
			loc.setLatitude(c.getDouble(i));
			loc.setLongitude(c.getDouble(c
					.getColumnIndex(Expedition.KEY_END_LONG)));
			// setDestLoc(loc);
		}

		setStartTS(c.getLong(c
				.getColumnIndex(Expedition.KEY_START_TIMESTAMP)));

		if ((i = DB.getIndex(c, Expedition.KEY_END_TIMESTAMP)) > -1)
			setEndTS(c.getLong(i));

		if ((i = DB.getIndex(c, Expedition.KEY_TREASURE_ID)) > -1)
			setPrize(Treasure.getTreasure(c.getInt(i)));
	}

	public ContentValues toContentValues() {
		ArrayList<String> keys = new ArrayList<String>();
		ArrayList<Object> vals = new ArrayList<Object>();

		keys.add(KEY_PID);
		vals.add(pID);

		keys.add(KEY_DISTANCE);
		vals.add(mDistance);

		keys.add(KEY_DIST_LEFT);
		vals.add(mDistanceLeft);

		if (mTravelled > 0) {
			keys.add(KEY_TRAVELED);
			vals.add(mTravelled);
		}
		// if (mStartLoc != null) {
		// keys.add(KEY_START_LAT);
		// vals.add(mStartLoc.getLatitude());
		// keys.add(KEY_START_LONG);
		// vals.add(mStartLoc.getLongitude());
		// }
		// if (mDestLoc != null) {
		// keys.add(KEY_END_LAT);
		// vals.add(mDestLoc.getLatitude());
		// keys.add(KEY_END_LONG);
		// vals.add(mDestLoc.getLongitude());
		// }
		if (mStartTS > 0) {
			keys.add(KEY_START_TIMESTAMP);
			vals.add(mStartTS);
		}
		if (mEndTS > 0) {
			keys.add(KEY_END_TIMESTAMP);
			vals.add(mEndTS);
			keys.add(KEY_TIME);
			vals.add(mEndTS - mStartTS);
		}
		if (mPrize != null) {
			keys.add(KEY_TREASURE_ID);
			vals.add(mPrize.getTID());
		}

		return DB.toContentValues(
				(String[]) keys.toArray(new String[keys.size()]),
				vals.toArray());
	}

	public static ContentValues toContentValues(Cursor c) {
		ContentValues kv = new ContentValues();

		kv.put(KEY_EID, c.getLong(c.getColumnIndex(KEY_EID)));
		kv.put(KEY_PID, c.getLong(c.getColumnIndex(KEY_PID)));
		kv.put(KEY_DISTANCE, c.getDouble(c.getColumnIndex(KEY_DISTANCE)));
		kv.put(KEY_TRAVELED, c.getDouble(c.getColumnIndex(KEY_TRAVELED)));
		kv.put(KEY_DIST_LEFT, c.getDouble(c.getColumnIndex(KEY_DIST_LEFT)));
		kv.put(KEY_START_LAT, c.getDouble(c.getColumnIndex(KEY_START_LAT)));
		kv.put(KEY_START_LONG, c.getDouble(c.getColumnIndex(KEY_START_LONG)));
		kv.put(KEY_END_LAT, c.getDouble(c.getColumnIndex(KEY_END_LAT)));
		kv.put(KEY_END_LONG, c.getDouble(c.getColumnIndex(KEY_END_LAT)));
		kv.put(KEY_START_TIMESTAMP,
				c.getLong(c.getColumnIndex(KEY_START_TIMESTAMP)));
		kv.put(KEY_END_TIMESTAMP,
				c.getLong(c.getColumnIndex(KEY_END_TIMESTAMP)));
		kv.put(KEY_TIME, c.getLong(c.getColumnIndex(KEY_TIME)));
		kv.put(KEY_TREASURE_ID, c.getInt(c.getColumnIndex(KEY_TREASURE_ID)));

		return kv;
	}

	/**************************/
	/** Interface Parcelable **/
	/**************************/

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(eID);
		dest.writeLong(pID);
		// dest.writeParcelable(mStartLoc, 0);
		// dest.writeParcelable(mDestLoc, 0);
		dest.writeDouble(mDistance);
		dest.writeDouble(mTravelled);
		dest.writeDouble(mDistanceLeft);
		dest.writeLong(mStartTS);
		dest.writeLong(mEndTS);
		dest.writeSerializable(mPrize);
	}

	public static final Parcelable.Creator<Expedition> CREATOR = new Parcelable.Creator<Expedition>() {
		public Expedition createFromParcel(Parcel in) {
			return new Expedition(in);
		}

		public Expedition[] newArray(int size) {
			return new Expedition[size];
		}
	};

	private Expedition(Parcel in) {
		eID = in.readLong();
		pID = in.readLong();
		// mStartLoc = in.readParcelable(null);
		// mDestLoc = in.readParcelable(null);
		mDistance = in.readDouble();
		mTravelled = in.readDouble();
		mDistanceLeft = in.readDouble();
		mStartTS = in.readLong();
		mEndTS = in.readLong();
		mPrize = (Treasure) in.readSerializable();
	}
}
