package nctuw.littledot.localtreasure.database;

import java.util.Random;

import nctuw.littledot.localtreasure.R;

public enum Treasure {
	WOOD(1, R.drawable.item_wood_24_24, "Wood"),
	LEATHER(2, R.drawable.item_leather_24_24, "Leather"),
	IRON_ORE(3, R.drawable.item_iron_ore_24_24, "Iron Ore");

	private final int mID;
	private final int mDrawableID;
	private final String mName;

	Treasure(int id, int drawID, String n) {
		mID = id;
		mDrawableID = drawID;
		mName = n;
	}

	public int getTID() {
		return mID;
	}

	public int getDrawableID() {
		return mDrawableID;
	}

	public String getName() {
		return mName;
	}

	public String toString() {
		return mName;
	}

	/**
	 * Generates a random reward.
	 * 
	 * @return
	 */
	public static Treasure generateReward(Expedition e) {
		if (e.getTravelled() < 300D)
			return null;

		Random rand = new Random();
		int index = 0;

		// different tiers of reward based on distance travelled?
		if (e.getTravelled() > 300D)
			index = rand.nextInt(values().length);

		return values()[index];
	}

	public static Treasure getTreasure(int id) {

		for (Treasure t : values()) {
			if (t.mID == id)
				return t;
		}
		throw new IllegalArgumentException("Treasure ID does not exist.");
	}
}
