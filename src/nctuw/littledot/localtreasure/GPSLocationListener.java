package nctuw.littledot.localtreasure;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class GPSLocationListener implements LocationListener {
	public static int	MSG_LOCATION_CHANGED	= 1;
	private Handler		mHandle;

	public GPSLocationListener(Handler handle) {
		mHandle = handle;
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		Message msg = mHandle.obtainMessage(MSG_LOCATION_CHANGED, location);
		mHandle.sendMessage(msg);
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
