package nctuw.littledot.localtreasure;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalCoordinates;

import android.location.Location;

public class Geodesy {

	public static Location calculateHaversineDestination(Location curLoc,
			double bearing, double distance) {

		double radLong = Math.toRadians(curLoc.getLongitude());
		double radLat = Math.toRadians(curLoc.getLatitude());
		double radBearing = Math.toRadians(bearing);

		double earthRadius = 6371009; // meter
		double angle = distance / earthRadius;

		double destLat = Math.asin(Math.sin(radLat) * Math.cos(angle)
				+ Math.cos(radLat) * Math.sin(angle) * Math.cos(radBearing));
		double destLong = radLong
				+ Math.atan2(
						Math.sin(radBearing) * Math.sin(angle)
								* Math.cos(radLat),
						Math.cos(angle) - Math.sin(radLat) * Math.sin(radLat));

		Location loc = new Location("TreasureHunter");
		loc.setLatitude(destLat);
		loc.setLongitude(destLong);
		return loc;
	}

	public static Location calculateVincentyDestination(Location curLoc,
			double bearing, double distance) {// meter
		// instantiate the calculator
		GeodeticCalculator geoCalc = new GeodeticCalculator();

		// select a reference elllipsoid
		Ellipsoid reference = Ellipsoid.WGS84;

		// set Lincoln Memorial coordinates
		GlobalCoordinates lincolnMemorial;
		lincolnMemorial = new GlobalCoordinates(curLoc.getLatitude(),
				curLoc.getLongitude());

		// find the destination
		double[] endBearing = new double[1];
		GlobalCoordinates dest = geoCalc.calculateEndingGlobalCoordinates(
				reference, lincolnMemorial, bearing, distance, endBearing);

		Location destLoc = new Location("TreasureHunter");
		destLoc.setLatitude(dest.getLatitude());
		destLoc.setLongitude(dest.getLongitude());
		return destLoc;
	}

}
