package nctuw.littledot.localtreasure;

import org.holoeverywhere.R;

public class Const {
	public static final String PACKAGE = "nctuw.littledot.localtreasure";
	public static final int THEME = R.style.Theme_Sherlock_Light_DarkActionBar;

	public static final String SETTINGS_EXIT_DIALOG = "settings_exitDialog";

	public static final String BUNDLE_SOURCE = "source";
	public static final String BUNDLE_EID = "eID";
	public static final String BUNDLE_PROFILE = "userProfile";
	public static final String BUNDLE_DISTANCE = "distance";
	public static final String BUNDLE_MODE = "mode";

	/* Expedition Mode */
	public static final int MODE_ONEWAY = 1;
	public static final int MODE_TOANDFRO = 2;
	public static final int MODE_MULTI = 3;
	public static final int MODE_FREESTYLE = 4;

}
