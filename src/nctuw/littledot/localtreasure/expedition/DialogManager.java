package nctuw.littledot.localtreasure.expedition;

import nctuw.littledot.localtreasure.database.Expedition;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;

import android.content.Context;
import android.content.DialogInterface;

public class DialogManager {

	public static Dialog onCreateDialog(Context context, Expedition expedition) {

		long seconds = (expedition.getEndTS() - expedition.getStartTS()) / 1000;

		String message = String.format(
				"You have come a long way to complete this expedition!\n"
						+ "You have found a %s in your travels.\n"
						+ "Distance travelled: %f meters\n" + "Time taken: %d seconds\n",
				new Object[] { expedition.getPrize().getName(),
						expedition.getTravelled(), seconds });

		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle("Congratulations!").setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		// Create the AlertDialog object and return it
		return builder.create();
	}
}
