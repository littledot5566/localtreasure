v Continue active expedition.
v Expedition list in ListView form.
v delete expedition from history.
* Help/About/Credits button telling users what this app is about, thanks for icons, etc.
* Expedition statistics. Graph of travelled distance over days, etc.
v Display treasure in a trophy case.
* Distance from destination, distance travelled in MapAct.
* "Displayable" column for Expeditions. Don't delete data. (do last)
v After completing an Expedition, remove from Active Expedition.
v Completed Expeditions should not display "Continue Expedition" option.
* Back button instead of icon.
* Different excuses when the log is empty. "Had a cold..." "It was raining..." etc.
* Implement sonar sounds when approaching treasure so that user doesn't have to look at the screen all the time.
* To-and-fro vs One-way mode
* Freedom mode
* Multiple-destination mode.
* Add expedition info in ExpeditionAcitivty. Use icon in action bar to trigger.