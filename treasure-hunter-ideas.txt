Android Application Treasure Hunter Ideas

The pitch:

Treasure Hunter is an app that promotes exercise by getting users out of their seats!
For to long have users been stranded inside their houses, we're here to save them from their couches!

The idea behind Treasure Hunter is similar to the childhood game Hot-Warm-Cold, where the protagonist, who does not know the location of the treasure, must depend on the other participants in the game to guide him/her by giving hints of either "hotter" or "colder".

In this app, the user first sets a distance bracket of how much he/she would like to travel. The application then randomly generates a destination where the user must travel to to unveil his treasures.

(Ex1) Distance brackets:
1-100m, 100-300m, 300-500m, 500-1km, 1-2km, etc.

Once the destination has been set and the user can proceed, the screen will display the distance between the user and the user's destination. As the user approaches the destination, the distance shrinks. Until a reasonable threshold (say 10% of the total distance to travel) is reached, the app notifies that the user is close enough and displays a "collect treasure" button like those cheesy phishing websites. The user finally taps the button to claim his hard-earned prize.

(Ex2) The "collect treasure" button
+--------------+
|Start Digging!|
+--------------+

This is just what we call "soft-core" mode. The user knows the exact distance to travel. We can kick that up a notch by introducing "hard-core" mode. Instaed of showing the exact distance, we only display keywords such as "arctic ice", "freezing cold", "cool", "warm", "hot", "burning sun", etc. as the user travels. Some problems could arise with this: if the target distance is super far (like 5km) and there are not enough identifiers (say 5), each identifier would have to span 1km, that would leave the user clueless where to go. Ideally, each identifier should span 100-200m so that users wouldn't be left walking in vain, but isn't that the point in the first place, walking for a virtual reward?

We can also kick it down a notch by creating "kiddie-mode", which basically includes a map and routes the kids' travel path.

(Ex3) Hard-core mode
+---------------------------------+
|Brr... It's freezing cold here...|
|Let's start moving...            |
|(no distance shown at all)       |
+---------------------------------+

In some cases, the location might be private property which the user cannot trespass, or on an island where you must swim accross. We're not that hard-core, so in cases where the destination cannot be reached, we offer an exit strategy so that your progress will not go into the drain. Accompanying the "collect treasure" button, we provide a "dig now" button that is shown at all times. The "dig now" button allows the user to dig for treasure where the user is, but of course the "value" of the prize will be at a discount relative to the distance travelled. So if the user were outside a private company building where the user's destination is and can't get in, tap the "dig now" button and collect the prize, which shouldn't be too shabby compared to the full prize because the user was almost there.

(Ex4) Unreachable destinations
+---------------------------+
|I'll settle for digging now|
+---------------------------+

Some people have negative opinions of where their destination. It might be a morgue, a cemetary, a nuclear power plant, etc. In these cases, we provide another exit strategy, an "I don't want to go there" button. The user is also rewarded accordingly similar to the unreachable case, but the application takes note of this decision and will never include the frowned-upon location in the pool of possible destination again.

(Ex5) Scary/Dangerous destinations
+------------------------------------------+
|This place is creepy... Get me outta here!|
+------------------------------+-----------+
|It's too dangerous to proceed!|
+------------------------------+


Business model:

This app can bring people to places, which means it can bring users/customers straight to your  door. Say you have a huge sale, you advertise using the traditional methods, but customers don't always go to your event, especially if you are a local shop owner. With this app, we can send customers your way.
Once there, give the user incentives to Tweet or share on Facebook by offering an extra prize or something, now you get more publicity, and it is through friends!



Roadblocks:

Data, data, data. Need location data of destinations. Google Places? OpenStreetMaps?
Android, Android, Android. Need to master Location & GPS protocols. Phone power consumption is also a great concern.
